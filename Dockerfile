FROM python:3.9.18-alpine
WORKDIR /app
COPY . /app
RUN pip3 install -r requirements.txt
CMD ["python3","hello.py"]  
